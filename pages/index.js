import {useState} from 'react'
import { Navbar } from '@/src/components/navbar.component'
import Button from "@/src/components/button.component";
import Modal from "@/src/components/modal.component";
import Image from 'next/image'

export default function Home() {
  const [visible, setVisible] = useState(false)
  
  function onChangeModal() {
    setVisible(!visible)
  }
  
  return (
    <div>
      <Navbar />
      <Image
        width={500}
        height={500}
        src={'/img.jpg'}
      />
        <Button
          htmlType={'button'}
          type={'default'}
          className={'btn-danger'}
          onClick={onChangeModal}
        >
          Open Modal
        </Button>
        <Modal
          visible={visible}
          onChange={onChangeModal}
        >
        <div className={'w-full flex items-center justify-between'}>
          <h3>Title Modal</h3>
          <Button
            htmlType={'button'}
            type={'default'}
            onClick={onChangeModal}
          >
            Close
          </Button>
        </div>

        </Modal>
      <Button
        htmlType={'button'}
        type={'primary'}
        className={'btn-primary'}
        onClick={(e) => {
          console.log(e, 'buttin 2');
        }}
      >Component Button</Button>
      <h1>Hallo</h1>
      <div className='h-screen w-full my-5'>
        <Image
          width={500}
          height={500}
          src={'/lazyload/1.jpg'}
        />
      </div>
      <div className='h-screen w-full my-5'>
        <Image
          width={500}
          height={500}
          src={'/lazyload/2.jpg'}
        />
      </div>
      <div className='h-screen w-full my-5'>
        <Image
          width={500}
          height={500}
          src={'/lazyload/3.jpg'}
        />
      </div>
      <div className='h-screen w-full my-5'>
        <Image
          width={500}
          height={500}
          src={'/lazyload/4.jpg'}
        />
      </div>
    </div>
  )
}